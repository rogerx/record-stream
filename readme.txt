record-stream.sh

Base implimentation for recording streams for a specified number of minutes.  

First, insert your stream URL within top declared variables, ensuring the 
ffmpeg stream output name has the correct filename suffix.

Secondly, ensure your destination save location exists and is properly defined 
within the top variables.  Test using the debug option below.

Finally, rename script to "record-CALL_LETTERS.sh", using
station call letters.

Eg.
$ mv record-stream.sh record-wtam.sh

For debugging,
$ DEBUG=1 record-stream.sh


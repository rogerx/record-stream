#!/bin/bash
#
# record-stream.sh
#
# AUTHOR: Roger Zauner (rdz), rogerx [dot] oss [AT] gmail [dot] com
# LICENCE: GPL-3 2023, Roger Zauner
#
# base implimentation, insert your unique stream URL within below top
# variables, then rename script to record-CALL_LETTERS.sh, using
# station call letters.
#
# Eg.
# record-wtam.sh
#
# Ensure the ffmpeg stream output name has the correct filename suffix.


set -o nounset          # Treat unset variables as an error
#shopt -s huponexit      # Allow killing this script with SIGUSR2
                        # (See trap below.)

debug_val="${DEBUG:=0}"     # 0 = no debug output, 1 = show debug output
                            # Or enable debug with:
                            # DEBUG=2 mkudffs-img.sh

# VARIABLES
#
# insert your stream url here
declare url="https://stream.revma.ihrhls.com/zc1749?streamid=1749&zip=44030"
declare filetype="m4a"
# set recording time span in minutes
declare -i record_time=1

# file naming
declare date=$(date +%Y%m%d)
declare time=$(date +%H%M)
# destination dir
# TODO: impliment dest_dir! switch var names for multi-platform?
#declare dest_dir="/home/roger/tv"
# path for recorded file save location
# linux or cygwin/windows option
declare filename_linux="${HOME}/tv/${date}-${time}-ch_wtam"
# use single quotes for win dir, then append variables, weird!
declare filename_cygwin='C:\cygwin65\home\roger\record\'"${date}-${time}-ch_wtam"

# leave empty
declare filename=""


# FUNCTIONS
#
debug()         # ifdef style debug
{
    if [ ${debug_val} -ne 0 ]; then
        "$@"
    fi
    # sample usage:
    # debug echo "variable  my_var=${my_var}"
    # debug echo "DEBUG: return value: ${?}"

}

echo()          # replace echo with safe drop-in printf
{
    local IFS=' '; printf '%s\n' "$*";
}

filename_platform() # check platform, choose destination filename
{
    debug echo "  DEBUG: In function filename_platform"
    
    # platform identification
    declare uname_str="$(uname -s)"

    debug echo "  DEBUG: uname_str = ${uname_str}"

    case "${uname_str}" in
        Linux*) filename="${filename_linux}" ;;

        CYGWIN*) filename="${filename_cygwin}" ;;

        esac

    debug echo "  DEBUG: filename = ${filename}"
}

record_stream()
{
    debug echo "  DEBUG: In function record_stream"
    debug echo "  DEBUG: ffmpeg url = ${url}"

    declare -gi pidof_record=0
    #declare -i i=0

    #i=i+1

    debug echo "  DEBUG: ffmpeg filename.filetype = ${filename}.${filetype}"
    # ffmpeg "reconnect 1" and "reconnect_at eof" are before other options!
    # reconnect_at_eof or reconnect_streamed causes problems with wkyc
    # live video streama!
    #
    # "-re" might help resolving:
    # Application provided invalid, non monotonically increasing dts to
    # muxer in stream 2: 8500433463 >= 5533308
    # av_interleaved_write_frame(): Invalid argument
    #
    # Some streams error on -reconnect_on_network_error 1
    # -reconnect_streamed 1 options
    #
    ffmpeg -reconnect 1 -reconnect_at_eof 1 -reconnect_on_network_error 1 -reconnect_streamed 1 \
      -hide_banner -loglevel warning \
      -protocol_whitelist file,http,https,tcp,tls,crypto \
      -i "${url}" \
      -vcodec copy -acodec copy -y "${filename}"."${filetype}" &
      
    #-vcodec copy -acodec copy -y "${_filename}"-"${i}"."${filetype}" &

    pidof_record="${!}"
    debug echo "  DEBUG: pidof_record = ${pidof_record}"

    # this might not be needed, if ffmpeg breaks, typically the script
    # will automatically break.
    if (( ${pidof_record} == 0 )) ; then
        echo "Error starting record/ffmpeg."
        exit 1
    fi

    echo "filename: ${filename}.${filetype}"
}

sleep_while_record()
{
    debug echo "  DEBUG: In function sleep_while_record"
    debug echo "  DEBUG: Recording/sleep time ${record_time} minutes"

    echo "recording time: ${record_time} minutes"

    sleep ${record_time}m
}


# MAIN
#

debug echo "  DEBUG: In main"

filename_platform

record_stream

sleep_while_record

debug echo "  DEBUG: kill pidof_record = ${pidof_record}"
kill ${pidof_record}

